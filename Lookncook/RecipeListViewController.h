//
//  RecipeListViewController.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-06.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface RecipeListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>
{
     NSFetchedResultsController *fetchedResultsController;   
}
@property (retain, nonatomic) IBOutlet UITableView *tableListView;
@property (retain,nonatomic ) NSArray *categories;
@property (retain, nonatomic) IBOutlet UIImageView *background;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property BOOL showFavorites;
@end
