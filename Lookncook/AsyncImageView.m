//
//  AsyncImageView.h
//
//  Created by Mattias Åkerman on 12/12/10.
//  Copyright 2010 Mattias Åkerman. You may use this source as you like
//  www.goodboy.se
//

#import "AsyncImageView.h"

// Example usage:
// AsyncImageView *av = [[AsyncImageView alloc] initWithFrame:self.view.frame];
// [av loadImageFromURL:@"http://www.goodboy.se/image.png" previewUrl:t@"http://www.goodboy.se/preview.png" cacheImage:NO];


@implementation AsyncImageView
@synthesize delegate,doScale,cacheKey;
@synthesize imageContentMode,connection,data;

-(id) initWithFrame:(CGRect)frame
{
    self = [ super initWithFrame: frame ];
    if ( self )
    {
        doScale = YES;
        imageContentMode = UIViewContentModeCenter;
        self.connection = nil;
    }
    return  self;
}

-(id) init
{
    self = [ super init ];
    if ( self )
    {
        
        doScale = YES;
    }
    return  self;
}

-(void) awakeFromNib 
{
    doScale = YES;
}


- (void)dealloc {
    [ delegate release ];
    
    if ( self.connection != nil )
        [self.connection cancel];
    self.connection = nil;
    
	self.data = nil;
	[theimage release];
	[activityView release];
	[filename release];
	[preview release];
	
    [super dealloc];
}

- (BOOL) isOk
{
	return ok;
}




- (void)loadImageFromURL:(NSString*)urlString previewUrl:(NSString*)previewURL cacheImageWithKey: ( NSString * ) key
{
    self.cacheKey = key;
    cacheWithKey = YES;
    [ self loadImageFromURL: urlString previewUrl: previewURL cacheImage: YES ];
    
}




- (void)loadImageFromURL:(NSString*)urlString previewUrl:(NSString*)previewURL cacheImage:(BOOL)cacheImage
{
    
    if ( self.connection != nil )
        [self.connection cancel];
    
	self.connection = nil;
    
	self.data = nil;

	if(cacheImage)
	{
        
        
        
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
		NSString *cahcesDirectory = [paths objectAtIndex:0];
	
        if ( cacheWithKey )
        {
            filename = [[NSString alloc] initWithString: cacheKey ];
        } else
        {
            filename = [[NSString alloc] initWithString:[urlString lastPathComponent]];
            
        }
        
		
        
	
		if( [[NSFileManager defaultManager] fileExistsAtPath:[cahcesDirectory stringByAppendingPathComponent:filename]] )
		{
			if ([[self subviews] count]>0) {
				[[[self subviews] objectAtIndex:0] removeFromSuperview];
			}
		
			NSMutableData *d = [[NSMutableData alloc] initWithContentsOfFile:[cahcesDirectory stringByAppendingPathComponent:filename]];
	
            float scale = 1.0;
            
            if ([UIScreen instancesRespondToSelector:@selector(scale)] && doScale ) {
                UIScreen* screen = [UIScreen mainScreen];
                scale = [ screen scale ];
            }
            
            UIImage *img = [UIImage imageWithData:d];
            
            img = [UIImage imageWithCGImage:img.CGImage scale:scale orientation:img.imageOrientation];
            
            
			theimage = [[UIImageView alloc] initWithImage:img ];
            
            
            theimage.contentMode = imageContentMode;
            
			theimage.alpha = 1.0;
            theimage.frame = self.bounds;  
			[d release];
		
			[self addSubview:theimage];
            
            if ( delegate != nil )
                [ delegate didDownloadImage: theimage ];
		
			return;
	
		}
	}
	
	
    if ( previewURL != nil )
    {
	preview = [[UIImageView alloc] initWithImage:[UIImage imageWithData:  [NSData dataWithContentsOfURL: [NSURL URLWithString: previewURL]]] ];
	preview.frame = self.bounds;
	//[self addSubview:preview];
    }
    
    NSString* escapedUrlString =
    [urlString stringByAddingPercentEscapesUsingEncoding:
     NSASCIIStringEncoding];

    NSLog( @"downloading: %@", escapedUrlString );
	
	NSURLRequest* request = [NSURLRequest requestWithURL:[[NSURL alloc] initWithString:escapedUrlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self]; //notice how delegate set to self object
    
    if ( connection == nil )
    {
        NSLog(@"yo");
    }
    
	ok = NO;
	
	
	activityView = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
	[self addSubview: activityView];
	activityView.center = CGPointMake( self.frame.size.width/2 ,self.frame.size.height/2);
	[activityView startAnimating];
	
	
	
}

- (void)connection:(NSURLConnection *)connection2 didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    
 
    
    if ( self.data != nil )
        [ self.data setLength:0];
    
    NSLog(@"Resetting!");
}


- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	if ( self.data==nil) { self.data = [[NSMutableData alloc] initWithCapacity:2048]; } 
	[ self.data appendData:incrementalData];
   // NSLog(@"recv");
}


- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	NSLog( @"fail" );
	[activityView stopAnimating];
	[activityView setAlpha:0.0];

}



- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
	 
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	NSString *cahcesDirectory = [paths objectAtIndex:0];
    
    NSLog( @"Filename: %@", [cahcesDirectory stringByAppendingPathComponent:filename]  );
	
	[ self.data writeToFile:[cahcesDirectory stringByAppendingPathComponent:filename] atomically:YES];
	
	self.connection=nil;
    
    float scale = 1.0;
    
    if ([UIScreen instancesRespondToSelector:@selector(scale)] && doScale) {
        UIScreen* screen = [UIScreen mainScreen];
        scale = [ screen scale ];
    }
    
    UIImage *img = [UIImage imageWithData:self.data];
    img = [UIImage imageWithCGImage:img.CGImage scale:scale orientation:img.imageOrientation];
	
	UIImageView* imageView = [[[UIImageView alloc] initWithImage: img] autorelease];
    imageView.contentMode = self.imageContentMode;
    imageView.alpha = 0.0;
    
    
    
	imageView.frame = self.bounds;
	[self addSubview:imageView];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(imageDidFadeOut:finished:context:)];
	imageView.alpha = 1.0;
	preview.alpha = 0.0;
	[UIView commitAnimations];
	
	[activityView stopAnimating];
	
	[imageView setNeedsLayout];
	[self setNeedsLayout];

	self.data = nil;
	ok = YES;
    
    if ( delegate != nil )
        [ delegate didDownloadImage:imageView ];
}



- (UIImage*) image {
	return [theimage image];
}

@end
