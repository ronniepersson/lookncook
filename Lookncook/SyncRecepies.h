//
//  SyncRecepies.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-06.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SyncRecepies : NSObject

-(void) doStartSync;
-(void) fetchUrl: ( NSURL * ) url;
-(void) parseRecepies: (NSArray *) recepies;

@property ( nonatomic, retain ) NSArray *categories;

@end
