//
//  LCSubCategoryTableViewCell.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-08.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardView.h"

@interface LCSubCategoryTableViewCell : UITableViewCell

@property (nonatomic, retain) CardView *cardView;
@property (nonatomic, retain) UIImageView *favPin;
@end
