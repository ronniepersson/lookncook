//
//  StoreObserver.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-05-08.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

#define kNoAdProductBought @"LOOKNCOOKNoAdProductBought"
#define kNoAdProductEnded @"LOOKNCOOKNoAdProductEnded"
#define kNoAdProductIdentifier @"removeads"
//#define kValidationServer @"https://sandbox.itunes.apple.com/verifyReceipt"
#define kValidationServer @"https://buy.itunes.apple.com/verifyReceipt"
#define kValidationSecret @"853e79d5929647d79122ed71e43d1e37"
@interface StoreObserver : NSObject<SKPaymentTransactionObserver>
+(BOOL) skipAd;
-(void) checkForValidReceipt;
@end
