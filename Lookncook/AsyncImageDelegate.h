//
//  AsyncImageDelegate.h
//  ica
//
//  Created by Ronnie Persson on 2011-05-12.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol AsyncImageDelegate <NSObject>
-(void) didDownloadImage: ( UIImageView *) imageView;
@end
