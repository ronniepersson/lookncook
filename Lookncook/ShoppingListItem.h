//
//  ShoppingListItem.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-28.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ShoppingListItem : NSManagedObject

@property (nonatomic, retain) NSString * item;
@property (nonatomic, retain) NSNumber * checked;

@end
