//
//  CardView.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-04-04.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "CardView.h"

@implementation CardView
@synthesize aSyncImage;
@synthesize label;

- (id)initWithFrame:(CGRect)frame
{
    return [ self initWithFrame: frame showBanner: YES ];
}

- (id)initWithFrame:(CGRect)frame showBanner: ( bool ) showBanner
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [ UIColor clearColor ];
        
        UIImageView *frame = [ [ UIImageView alloc ] initWithFrame: CGRectMake( 0, 0, self.frame.size.width, self.frame.size.height ) ];
        frame.contentMode = UIViewContentModeTop;
        UIImageView *banner = [ [ UIImageView alloc ] init ];
        if ( self.frame.size.width <= 70 )
        {
            frame.image = [ UIImage imageNamed: @"foto_liten.png" ];
            self.aSyncImage = [ [ AsyncImageView alloc ] initWithFrame: CGRectMake( 5 , 5, 60, 60 ) ];
            
            self.label = [ [ UILabel alloc ] init ];
            self.label.hidden = YES;
            banner.hidden = YES;
            
        } else {
            frame.image = [ UIImage imageNamed: @"foto_stor.png" ];
            self.aSyncImage = [ [ AsyncImageView alloc ] initWithFrame: CGRectMake(  5, 5, self.frame.size.width - 10, self.frame.size.width - 10 ) ];
            self.label = [ [ UILabel alloc ] initWithFrame: CGRectMake( 5, self.frame.size.height - 25, self.frame.size.width - 10, 10 ) ];
            self.label.font = [ UIFont fontWithName: @"Oswald-Bold" size: 9.0 ];
            self.label.textAlignment = UITextAlignmentCenter;
            
            if ( showBanner )
            {
                banner.image = [ UIImage imageNamed: @"new.png" ];
                banner.frame = CGRectMake(0, 0, banner.image.size.width, banner.image.size.height );
            } else {
                banner.hidden = NO;
            }
            
        }
        self.label.backgroundColor = [ UIColor clearColor ];
        aSyncImage.imageContentMode = UIViewContentModeScaleAspectFill;
        self.aSyncImage.clipsToBounds = YES;
        [self addSubview: frame ];
        [ self addSubview: aSyncImage ];
        [ self addSubview: label ];
        [ self addSubview: banner ];
        [ frame release ];

    }
    return self;
}


-(void) awakeFromNib
{
    
        
    
    
}

-(void) dealloc{
    self.label = nil;
    self.aSyncImage = nil;
    
    [ super dealloc ];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
