//
//  CookingViewController.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-10.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "CookingViewController.h"
#import "AsyncImageView.h"
#import "LCAppDelegate.h"
#import "ShoppingListItem.h"
#import "ShoppingListViewController.h"
#import "StoreObserver.h"
#import <QuartzCore/QuartzCore.h>

#define ACTIONSHEETBUTTON_ADDTOGROSSERY 0
#define ACTIONSHEETBUTTON_SETASFAVORITE 1

@implementation CookingViewController
@synthesize contentView;
@synthesize pageControlView;
@synthesize pageControl;
@synthesize buttonView;
@synthesize buttonViewVertical;
@synthesize title;
@synthesize shoppingListView;
@synthesize shoppingListList;
@synthesize shoppingListContentView;
@synthesize shoppingListHeader;
@synthesize videoRecepieView;
@synthesize videoRecepiePreview;
@synthesize videoText;
@synthesize stepByStepView;
@synthesize stepByStepContentView;
@synthesize steByStepHeader;
@synthesize stepByStepText;
@synthesize orientationSign;
@synthesize favButton;
@synthesize recipe;
@synthesize player;
@synthesize lastPlaybackTime;
@synthesize titleHeader;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.lastPlaybackTime = 0.0;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}




#pragma mark - Async image delegate

-(void) didDownloadImage:(UIImageView *)imageView
{
}

#pragma mark - Scroll Delegate
-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [ TestFlight passCheckpoint: NSStringFromClass( [ self class ] ) ];
    
    AsyncImageView *async = [ [ AsyncImageView alloc ] init ];
    async.frame = CGRectMake( 0, 0, self.videoRecepiePreview.frame.size.width,  self.videoRecepiePreview.frame.size.height );
    async.delegate = self;
    async.imageContentMode = UIViewContentModeScaleAspectFill;
    [ self.videoRecepiePreview addSubview: async ];
    [ async loadImageFromURL: recipe.img  previewUrl: nil cacheImageWithKey: [ NSString stringWithFormat: @"full_%@.png", recipe.id] ];
    [ async release ];
    
    self.titleHeader.text = [self.recipe.title uppercaseString];
    self.titleHeader.font = [ UIFont fontWithName: @"Oswald-Bold" size: 16 ];
    
    self.videoText.font = [ UIFont fontWithName: @"Oswald-Bold" size: 16 ];
   
    
    self.shoppingListList.font = [ UIFont fontWithName: @"Asap-Regular" size: 14 ];
    self.shoppingListList.text = self.recipe.ingredients;
    self.shoppingListHeader.font = [ UIFont fontWithName: @"Oswald-Bold" size: 16 ];
    self.shoppingListContentView.layer.cornerRadius = 5.0f;
    
    
    self.stepByStepText.font = [ UIFont fontWithName: @"Asap-Regular" size: 14 ];
    self.stepByStepText.text = self.recipe.cooking;
    
    self.steByStepHeader.font = [ UIFont fontWithName: @"Oswald-Bold" size: 16 ];
    self.stepByStepContentView.layer.cornerRadius = 5.0f;
    
    [ self.navigationItem setTitleView: [[ UIImageView alloc ] initWithImage: [UIImage imageNamed: @"lclogo.png"]]];
    
    [self.pageControlView addSubview:self.videoRecepieView];
    [self.pageControlView addSubview:self.shoppingListView];
    [self.pageControlView addSubview:self.stepByStepView];
    
    self.videoRecepieView.frame = CGRectMake( 0, 0, self.pageControlView.frame.size.width, self.pageControlView.frame.size.height);
    self.shoppingListView.frame = CGRectMake( self.pageControlView.frame.size.width * 1, 0, self.pageControlView.frame.size.width, self.pageControlView.frame.size.height);

    self.stepByStepView.frame = CGRectMake( self.pageControlView.frame.size.width * 2, 0, self.pageControlView.frame.size.width, self.pageControlView.frame.size.height);
        
    
    [self.pageControlView setContentSize: CGSizeMake( self.pageControlView.frame.size.width * 3, self.pageControlView.frame.size.height )];
    
    UIBarButtonItem *btn = [[ UIBarButtonItem alloc ] initWithImage: [ UIImage imageNamed:@"shoppinglist.png"] style:UIBarButtonItemStylePlain target: self action: @selector( showShoppingList ) ];
    
    self.navigationItem.rightBarButtonItem = btn;
    [btn release];
    
    if ( [self.recipe.isFavorite boolValue] )
        [ favButton setBackgroundImage: [ UIImage imageNamed: @"favbutton.png" ] forState: UIControlStateNormal ];
    else
        [ favButton setBackgroundImage: [ UIImage imageNamed: @"favbutton_unfav.png" ] forState: UIControlStateNormal ];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(playbackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object: nil ];
    
    self.player = nil;
    
    // Do any additional setup after loading the view from its nib.
}



-(void) viewWillAppear:(BOOL)animated
{
    
    [ self willRotateToInterfaceOrientation: self.interfaceOrientation duration: 0 ];
    [ self willAnimateRotationToInterfaceOrientation: self.interfaceOrientation duration: 0 ];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [ self.player stop ];
    [ self.player.view removeFromSuperview ];
}

- (void)viewDidUnload
{
    [self setContentView:nil];
    [self setShoppingListView:nil];
    [self setVideoRecepieView:nil];
    [self setVideoRecepiePreview:nil];
    [self setShoppingListList:nil];
    [self setButtonView:nil];
    [self setStepByStepView:nil];
    [self setStepByStepText:nil];
    [self setButtonViewVertical:nil];
    [self setPageControl:nil];
    [self setPageControlView:nil];
    [self setVideoText:nil];
    [self setShoppingListContentView:nil];
    [self setShoppingListHeader:nil];
    [self setSteByStepHeader:nil];
    [self setStepByStepContentView:nil];
    [self setTitleHeader:nil];
    [self setFavButton:nil];
    [self setOrientationSign:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return player != nil || !self.orientationSign.hidden;
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    if ( toInterfaceOrientation == UIInterfaceOrientationPortrait )
        self.orientationSign.hidden = YES;
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
   
    int width = 320;
    int height = 480;
    
    if ( toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight )
    {
        width = 480;
        height = 320;
    } 

    [player.view setFrame: CGRectMake( 0, 0, width, height ) ];
    self.orientationSign.frame = CGRectMake( 0, 0, width, height );
    
    if ( self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft || self.interfaceOrientation == UIInterfaceOrientationLandscapeRight )
    {
        
    }
}

- (void)dealloc {
    [contentView release];
    [shoppingListView release];
    [videoRecepieView release];
    [videoRecepiePreview release];   
    [shoppingListList release];
    [buttonView release];
    [stepByStepView release];
    [stepByStepText release];
    [buttonViewVertical release];
    self.player = nil;
    self.recipe = nil;
    [pageControl release];
    [pageControlView release];
    [videoText release];
    [shoppingListContentView release];
    [shoppingListHeader release];
    [steByStepHeader release];
    [stepByStepContentView release];
    [titleHeader release];
    [favButton release];
    [orientationSign release];
    [super dealloc];
}

- (IBAction)selectStepByStep:(id)sender {
    
    [ TestFlight passCheckpoint: @"Changed tab to Step By Step" ];
    
    if ( [ [ self.contentView subviews ] count ] >  0 )
        [[[ self.contentView subviews ] objectAtIndex: 0 ] removeFromSuperview];
    [ self.contentView addSubview: self.stepByStepView ];
}

- (IBAction)selectShoppingList:(id)sender {
    
    [ TestFlight passCheckpoint: @"Changed tab to Shoppinglist" ];
    
    if ( [ [ self.contentView subviews ] count ] >  0 )
        [[[ self.contentView subviews ] objectAtIndex: 0 ] removeFromSuperview];
    [ self.contentView addSubview: self.shoppingListView ];
}

- (IBAction)selectVideoRecepie:(id)sender {
    
    [ TestFlight passCheckpoint: @"Changed tab to Video" ];
    
    if ( [ [ self.contentView subviews ] count ] >  0 )
        [[[ self.contentView subviews ] objectAtIndex: 0 ] removeFromSuperview];
    [ self.contentView addSubview: self.videoRecepieView ];
}

-(void) playbackDidFinish: (id) sender
{
    if ( self.player.currentPlaybackTime == self.player.duration )
    {
        [ TestFlight passCheckpoint: @"Video playback finished" ];
        self.lastPlaybackTime = 0.0f;
    }
    else
    {
        [ TestFlight passCheckpoint: @"Video playback paused" ];
        self.lastPlaybackTime = self.player.currentPlaybackTime;
    }
    
    if ( self.interfaceOrientation != UIInterfaceOrientationPortrait )
    {
        self.orientationSign.hidden = NO;
    }
    
    [ self.player.view removeFromSuperview ];
    self.player = nil;
}

- (IBAction)startPlay:(id)sender {
    
    
    [ TestFlight passCheckpoint: @"Video playback started" ];
    
    NSURL *url = [ NSURL URLWithString: self.recipe.video ];
    
    self.player =
    [[MPMoviePlayerController alloc] initWithContentURL: url];
    player.controlStyle = MPMovieControlStyleFullscreen;
    player.initialPlaybackTime = self.lastPlaybackTime;
    [player prepareToPlay];
    [player.view setFrame: self.view.window.rootViewController.view.bounds];  // player's frame must match parent's
    [ self.view.window.rootViewController.view addSubview: player.view];
}

- (IBAction)changeSection:(id)sender {
    pageControlUsed = YES;
    [ self.pageControlView scrollRectToVisible: CGRectMake( pageControl.currentPage * pageControlView.frame.size.width , 0, pageControlView.frame.size.width, pageControlView.frame.size.height) animated: YES ];
}

- (IBAction)setAsFav:(id)sender {
    LCAppDelegate *appDelegate;
    NSError *error;
    [ TestFlight passCheckpoint: @"Recipie favored" ];
    appDelegate = ( LCAppDelegate * ) [ [ UIApplication sharedApplication ] delegate ];
    
    self.recipe.isFavorite = [ NSNumber numberWithBool: ![ self.recipe.isFavorite boolValue ] ];
    [ appDelegate.managedObjectContext save: &error ];
    
    if ( [self.recipe.isFavorite boolValue] )
        [ favButton setBackgroundImage: [ UIImage imageNamed: @"favbutton.png" ] forState: UIControlStateNormal ];
    else
        [ favButton setBackgroundImage: [ UIImage imageNamed: @"favbutton_unfav.png" ] forState: UIControlStateNormal ];
    
    
    [[ NSNotificationCenter defaultCenter ] postNotificationName: @"favorites" object: nil ];
    

}

- (IBAction)addToShoppingList:(id)sender {
    LCAppDelegate *appDelegate;
    NSError *error;
    NSEntityDescription *entity;
    [ TestFlight passCheckpoint: @"Added ingredients to shopping list" ];
    appDelegate = ( LCAppDelegate * ) [ [ UIApplication sharedApplication ] delegate ];
    entity = [ NSEntityDescription entityForName: @"ShoppingListItem" inManagedObjectContext: appDelegate.managedObjectContext ];
    for ( NSString *line in [ self.recipe.ingredients componentsSeparatedByString: @"\n" ] )
    {
        if ( [ line length ] > 0 )
        {
            ShoppingListItem *s = [[ ShoppingListItem alloc ] initWithEntity: entity insertIntoManagedObjectContext: appDelegate.managedObjectContext ];
            s.checked = [ NSNumber numberWithBool: NO ];
            s.item = line;
        }
    }
    
    [ appDelegate.managedObjectContext save: &error ];
}


-(void) showShoppingList
{
    ShoppingListViewController *svc = [ [ ShoppingListViewController alloc ] initWithNibName: @"ShoppingListViewController" bundle:nil ];
    [ self presentModalViewController: svc animated: YES ];
    [ svc release ];
}


@end
