//
//  RecipeListViewController.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-06.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "RecipeListViewController.h"
#import "LCAppDelegate.h"
#import "Recipe.h"
#import "LCSubCategoryTableViewCell.h"
#import "CookingViewController.h"
#import "ShoppingListViewController.h"
#import "GADBannerView.h"
#import "StoreObserver.h"

@implementation RecipeListViewController
@synthesize tableListView;
@synthesize fetchedResultsController;
@synthesize categories;
@synthesize background;
@synthesize showFavorites;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.showFavorites = NO;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void) showShoppingList
{
    ShoppingListViewController *svc = [ [ ShoppingListViewController alloc ] initWithNibName: @"ShoppingListViewController" bundle:nil ];
    [ self presentModalViewController: svc animated: YES ];
    [ svc release ];
}


#pragma mark -
#pragma mark NSFetchedResultsControllerDelegate

- (NSFetchedResultsController *)fetchedResultsController {
    
    if ( fetchedResultsController != nil) {
        return fetchedResultsController;
    }
    
    LCAppDelegate *appDelegate = (LCAppDelegate *) [ [ UIApplication sharedApplication ] delegate ];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Recipe" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    if ( showFavorites )
    {
        NSPredicate *predicate = [ NSPredicate predicateWithFormat: @"isFavorite==%@", [ NSNumber numberWithBool: YES ] ];
        [ fetchRequest setPredicate: predicate ];
    } else
    {
        NSPredicate *predicate = [ NSPredicate predicateWithFormat: @"category.id IN %@", self.categories ];
        [ fetchRequest setPredicate: predicate ];
    }
    
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] 
                                        initWithKey:@"title" ascending:YES]; 
    NSArray *sortDescriptors = [[NSArray alloc] 
                                initWithObjects:sortDescriptor2, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = 
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext: appDelegate.managedObjectContext 
                                          sectionNameKeyPath: nil cacheName:nil];
    
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    [aFetchedResultsController release];
    [fetchRequest release];
    [sortDescriptor2 release];
    [sortDescriptors release];
    
    return fetchedResultsController;
} 


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableListView reloadData];
}


#pragma mark - Table view data source



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return[ [fetchedResultsController sections] count ];
}

-(BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [ [fetchedResultsController sections] objectAtIndex: section ];
    NSLog( @"Rows %d", [ sectionInfo numberOfObjects ] );
    return  [ sectionInfo numberOfObjects ];
}



- (UITableViewCell *)tableView:(UITableView *)tableViewer cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"cell";
    
    LCSubCategoryTableViewCell *cell = [tableViewer dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[LCSubCategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
        
    Recipe *recipe = [ fetchedResultsController objectAtIndexPath: indexPath ];
    
    
    cell.favPin.hidden = ![recipe.isFavorite boolValue];
    cell.textLabel.text = recipe.title;
    [cell.cardView.aSyncImage loadImageFromURL: recipe.thumbnail previewUrl: nil cacheImage: YES ];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ( showFavorites )
            [ TestFlight passCheckpoint: @"Showed recipie from favorite" ];
    else
            [ TestFlight passCheckpoint: @"Showed recipie from menu" ];
    
    CookingViewController *cvc = [ [ CookingViewController alloc ] initWithNibName:@"CookingViewController"  bundle: nil ];
    
    cvc.recipe = [ fetchedResultsController objectAtIndexPath: indexPath ];
    
    [ self.navigationController pushViewController: cvc animated: YES ];
    
    [ cvc release ];
    
    
   // self.fetchedResultsController = nil;
    
}


-(UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.showFavorites;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
        
            Recipe *recipe = [ fetchedResultsController objectAtIndexPath: indexPath ];
            recipe.isFavorite = NO;
            
            
       
       // [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        
    }    
    
    
}

-(void) removeAd
{
    [[ self.view viewWithTag: 9911 ] removeFromSuperview ];
    self.tableListView.frame = CGRectMake( self.view.frame.origin.x,self.view.frame.origin.x, self.view.frame.size.width, 480-20-44 );
    
}

-(void) addAdToView
{
    
    self.tableListView.frame = CGRectMake( self.view.frame.origin.x,self.view.frame.origin.x, self.view.frame.size.width, 480-20-44-44 );
    GADBannerView *adView =
    [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    adView.rootViewController = self;
    adView.adUnitID = @"a14fa63600bb900";
    adView.frame = CGRectMake( 0, 480-20-44-kGADAdSizeBanner.size.height, kGADAdSizeBanner.size.width, kGADAdSizeBanner.size.height);
    adView.tag = 9911;
    // Place the ad view onto the screen.
    [self.view addSubview:adView];
    [adView release];
    
    
    // Request an ad without any additional targeting information.
    [adView loadRequest:nil];
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ( ![ StoreObserver skipAd ] )
    {
        [ self addAdToView ];
    } else {
        [ self removeAd ];
    }
    
    [ TestFlight passCheckpoint: NSStringFromClass( [ self class ] ) ];
    
    [self.tableListView setShowsVerticalScrollIndicator: NO ];
    
    
    
    
    UIBarButtonItem *btn = [[ UIBarButtonItem alloc ] initWithImage: [ UIImage imageNamed:@"shoppinglist.png"] style:UIBarButtonItemStylePlain target: self action: @selector( showShoppingList ) ];
    
    self.navigationItem.rightBarButtonItem = btn;
    [btn release];
    
    [ self.navigationItem setTitleView: [[ UIImageView alloc ] initWithImage: [UIImage imageNamed: @"lclogo.png"]]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector( removeAd ) name:kNoAdProductBought object: nil ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector( addAdToView ) name:kNoAdProductEnded object: nil ];
    
    

    
    
    return;
    // Do any additional setup after loading the view from its nib.
}


- (void)viewDidUnload
{
    [self setTableListView:nil];
    [self setBackground:nil];
    [super viewDidUnload];
    self.fetchedResultsController = nil;
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void) viewWillAppear:(BOOL)animated
{
    NSError *error;
    [ self.fetchedResultsController performFetch: &error ];
    
    [ self willAnimateRotationToInterfaceOrientation: self.interfaceOrientation duration: 0 ];
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
    int width = 320;
    int height = 480 - 44 - 20 - ( [ StoreObserver skipAd ] ? 0 : kGADAdSizeBanner.size.height );
    
    if ( toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight )
    {
        width = 480;
        height = 320 - 44 - 20 - ( [ StoreObserver skipAd ] ? 0 : kGADAdSizeBanner.size.height );
    }
    
    self.background.frame = CGRectMake( 0, self.tableListView.frame.origin.y, width, 500);
    
    self.tableListView.frame = CGRectMake( self.tableListView.frame.origin.x , self.tableListView.frame.origin.y , width , height);

}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return interfaceOrientation == UIInterfaceOrientationPortrait;
}

- (void)dealloc {
    [ [ NSNotificationCenter defaultCenter ] removeObserver: self ];
    self.fetchedResultsController = nil;
    [tableListView release];
    [background release];
    [super dealloc];
}
@end
