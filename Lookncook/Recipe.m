//
//  Recipe.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-05-11.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "Recipe.h"
#import "Category.h"


@implementation Recipe

@dynamic cooking;
@dynamic cookingTime;
@dynamic created;
@dynamic id;
@dynamic img;
@dynamic ingredients;
@dynamic isFavorite;
@dynamic title;
@dynamic tools;
@dynamic video;
@dynamic thumbnail;
@dynamic category;

@end
