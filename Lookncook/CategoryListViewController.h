//
//  CategoryListViewController.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-08.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <StoreKit/StoreKit.h>
#import "CardView.h"
@interface CategoryListViewController : UIViewController< UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UISearchBarDelegate,SKProductsRequestDelegate , UIAlertViewDelegate >
{
    BOOL stopScroll;
    int purchaseType;
    int selectedIndex;
}
@property (retain, nonatomic) IBOutlet UITableView *tableListView;
@property (retain, nonatomic) IBOutlet UIImageView *background;
@property (retain, nonatomic) NSArray *categories;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UIScrollView *latestRecipies;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UITableView *searchTableView;
@property (retain, nonatomic) IBOutlet UIView *searchVIew;
@property (retain, nonatomic) IBOutlet UISearchBar *searchBar;
@property ( retain, nonatomic ) NSArray *searchResult;
@property (retain, nonatomic) IBOutlet UIButton *subButton;

- (IBAction)purchase:(id)sender;
@end
