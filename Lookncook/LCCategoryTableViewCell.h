//
//  LCCategoryTableViewCell.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-08.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LCCategoryTableViewCell : UITableViewCell
@property (nonatomic, retain) UIImageView *background;
@property ( nonatomic,retain) UIImageView *icon;

@end
