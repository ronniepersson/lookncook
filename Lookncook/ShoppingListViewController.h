//
//  ShoppingListViewController.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-28.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface ShoppingListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate,UIActionSheetDelegate>
{
    NSFetchedResultsController *fetchedResultsController;   
}

@property (retain, nonatomic) IBOutlet UINavigationItem *infoNavigationItem;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *actionButton;
@property (retain, nonatomic) IBOutlet UITableView *tableListView;
@property (retain, nonatomic) IBOutlet UIImageView *background;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
- (IBAction)done:(id)sender;
-(IBAction ) showOptions: ( id ) sender;
@end
