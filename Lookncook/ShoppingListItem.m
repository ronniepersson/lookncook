//
//  ShoppingListItem.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-28.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "ShoppingListItem.h"


@implementation ShoppingListItem

@dynamic item;
@dynamic checked;

@end
