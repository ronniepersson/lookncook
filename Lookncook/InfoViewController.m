//
//  InfoViewController.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-05-07.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "InfoViewController.h"
#import "StoreObserver.h"
#import <QuartzCore/QuartzCore.h>

@interface InfoViewController ()

@end

@implementation InfoViewController
@synthesize infoNavigationItem;
@synthesize infoWebView;
@synthesize bgview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.bgview.layer.cornerRadius = 5.0f;
    [self.infoWebView loadRequest: [ NSURLRequest requestWithURL: [ NSURL URLWithString: @"http://www.lookncook.se/api/aboutus" ] ] ];
    // Do any additional setup after loading the view from its nib.
    [ self.infoNavigationItem setTitleView: [[ UIImageView alloc ] initWithImage: [UIImage imageNamed: @"lclogo.png"]]];
    
}

- (void)viewDidUnload
{
    [self setInfoWebView:nil];
    [self setBgview:nil];
    [self setInfoNavigationItem:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [infoWebView release];
    [bgview release];
    [infoNavigationItem release];
    [super dealloc];
}
- (IBAction)done:(id)sender {
    [ self dismissModalViewControllerAnimated: YES ];
}



@end
