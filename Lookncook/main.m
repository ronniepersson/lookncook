//
//  main.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-06.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LCAppDelegate class]));
    }
}
