//
//  ArticleTableViewCell.m
//  Pluto
//
//  Created by Ronnie Persson on 2012-01-27.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "LCCategoryTableViewCell.h"

@implementation LCCategoryTableViewCell
@synthesize icon,background;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
     
        self.background = [[ UIImageView alloc ] initWithImage: [ UIImage imageNamed: @"cellbg.png" ] ];
            
        
        [ self addSubview: background ];
        [ self sendSubviewToBack: background ];
        
        self.backgroundColor = [ UIColor clearColor ];
        
        
        
        self.icon = [[ UIImageView alloc ] initWithImage: [ UIImage imageNamed: @"icon_1.png" ] ];
         [ self addSubview: icon ];
        
        self.textLabel.textColor = [ UIColor darkGrayColor ];
        self.textLabel.font = [ UIFont italicSystemFontOfSize: 20 ];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return self;
}

-(void) layoutSubviews
{
    [ super layoutSubviews ];
    
    self.textLabel.frame = CGRectMake( 120, 0, 320-120, self.frame.size.height );
    
    background.frame = CGRectMake( 0, 0, 320, 40 );
    
    
    self.icon.frame = CGRectMake( 95/2, 0, self.icon.image.size.width, self.icon.image.size.height );
        
    
    
}




-(void) dealloc
{
    self.icon = nil;
    self.background = nil;
    [ super dealloc ];
}

@end
