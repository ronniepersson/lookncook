//
//  LCSubCategoryTableViewCell.m
//  Pluto
//
//  Created by Ronnie Persson on 2012-01-27.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "LCSubCategoryTableViewCell.h"

@implementation LCSubCategoryTableViewCell
@synthesize cardView;
@synthesize favPin;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        UIView *v = [ [ UIView alloc ] initWithFrame: CGRectMake( 0, 0, 320, 84 ) ];
        v.backgroundColor = [ UIColor colorWithWhite: 1.0 alpha: 0.5 ];
        [ self addSubview: v ];
        [ self sendSubviewToBack: v ];
        
        CardView *cv = [ [ CardView alloc ] initWithFrame: CGRectMake(5, 5, 70, 74 ) ];
        self.cardView = cv;
        [ cv release ];
        
        [ self addSubview: cardView ];
        
        UIImageView *fp = [ [ UIImageView alloc ] initWithFrame: CGRectMake( 70, 0, 18, 16 ) ];
        self.favPin = fp;
        self.favPin.image = [ UIImage imageNamed: @"pin.png" ];
        [ fp release ];
        
        [ self addSubview: favPin ];
        
        self.textLabel.lineBreakMode = UILineBreakModeTailTruncation;
        self.textLabel.numberOfLines = 1;
     
        self.textLabel.textColor = [ UIColor blackColor ];
        self.textLabel.font = [ UIFont fontWithName: @"Oswald-Bold" size: 24 ];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}


-(void) layoutSubviews
{
    self.favPin.frame = CGRectMake( 320-35-6, 84/2-31/2,  35, 31 );
    self.textLabel.frame = CGRectMake(90,0,320-90-41,84);
}



-(void) dealloc
{
    self.cardView = nil;
    [ super dealloc ];
}

@end
