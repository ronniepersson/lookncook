//
//  AsyncImageView.h
//
//  Created by Mattias Åkerman on 12/12/10.
//  Copyright 2010 Mattias Åkerman. You may use this source as you like
//  www.goodboy.se
//

#import <UIKit/UIKit.h>
#import "AsyncImageDelegate.h"

@interface AsyncImageView : UIView {
	
	UIActivityIndicatorView *activityView;
	
	NSString *filename;
    NSString *cacheKey;
	
	BOOL ok;
    BOOL cacheWithKey;
	UIViewController<AsyncImageDelegate> *delegate;
	UIImageView *theimage;
	UIImageView *preview;
    
    BOOL doScale;
    UIViewContentMode imageContentMode;
}
@property ( assign ) UIViewContentMode imageContentMode;
@property ( nonatomic, retain ) UIViewController<AsyncImageDelegate> *delegate;
@property ( assign ) BOOL doScale;
@property ( nonatomic, retain ) NSString *cacheKey;
@property ( nonatomic, retain ) NSURLConnection* connection;
@property ( nonatomic, retain ) NSMutableData* data; 
- (void)loadImageFromURL:(NSString*)url previewUrl:(NSString*)previewURL cacheImage:(BOOL)cacheImage;
- (void)loadImageFromURL:(NSString*)urlString previewUrl:(NSString*)previewURL cacheImageWithKey: ( NSString * ) key;

- (UIImage*) image;
- (BOOL) isOk;

@end
