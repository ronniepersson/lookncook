//
//  LCSubCategoryTableViewCell.m
//  Pluto
//
//  Created by Ronnie Persson on 2012-01-27.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "LCShoppingItemTableViewCell.h"

@implementation LCShoppingItemTableViewCell
@synthesize background;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        self.background = [[ UIImageView alloc ] initWithImage: [ UIImage imageNamed: @"cellbg.png" ] ];
        
        
        [ self addSubview: background ];
        [ self sendSubviewToBack: background ];
        
        self.backgroundColor = [ UIColor clearColor ];
        self.textLabel.textColor = [ UIColor darkGrayColor ];
        self.textLabel.font = [ UIFont italicSystemFontOfSize: 15 ];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void) layoutSubviews
{
    [ super layoutSubviews ];
    self.textLabel.frame = CGRectMake( 50, 0, 320-50*2, self.frame.size.height );
    background.frame = CGRectMake( 0, 0, 320, self.frame.size.height );
    
}




-(void) dealloc
{
    self.background = nil;
    [ super dealloc ];
}

@end
