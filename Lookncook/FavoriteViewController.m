//
//  FavoriteViewController.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-04-13.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "FavoriteViewController.h"
#import "LCAppDelegate.h"
#import "Recipe.h"
#import "CardView.h"
#import "CookingViewController.h"
@implementation FavoriteViewController
@synthesize scrollView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-( Recipe * ) recepieWithId: ( NSNumber *) id
{
    LCAppDelegate *appDelegate = (LCAppDelegate *) [ [ UIApplication sharedApplication ] delegate ];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Recipe" inManagedObjectContext: appDelegate.managedObjectContext ];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] 
                                        initWithKey:@"id" ascending:YES];
    
    
    NSArray *sortDescriptors = [[NSArray alloc] 
                                initWithObjects: sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate *predicate = [ NSPredicate predicateWithFormat: @"id==%@", id ];
    
    [ fetchRequest setPredicate: predicate ];
    
    NSError *error;
    NSArray *res = [ appDelegate.managedObjectContext executeFetchRequest: fetchRequest error: &error ];  
    
    if ( res != nil )
        if ( [ res count ] > 0 )
            return [ res objectAtIndex: 0 ];
    
    return nil;
}




-(void) setupFavorites
{
    LCAppDelegate *appDelegate = (LCAppDelegate *) [ [ UIApplication sharedApplication ] delegate ];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Recipe" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    NSPredicate *predicate = [ NSPredicate predicateWithFormat: @"isFavorite==%@", [ NSNumber numberWithBool: YES ] ];
    [ fetchRequest setPredicate: predicate ];
   
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] 
                                        initWithKey:@"category" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] 
                                         initWithKey:@"title" ascending:YES]; 
    NSArray *sortDescriptors = [[NSArray alloc] 
                                initWithObjects:sortDescriptor,sortDescriptor2, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    
    NSArray *favorites = [ appDelegate.managedObjectContext executeFetchRequest: fetchRequest error: &error ];
    
    
    for ( UIView *v in self.scrollView.subviews )
    {
        [ v removeFromSuperview ];
    }
    
    int colNr = 0;
    int rowNr = 0;
    
    int n= 0;
    for ( Recipe *r in favorites  )
    {
        
        CardView *view = [ [ CardView alloc ] initWithFrame: CGRectMake( 5+colNr*107, 5+rowNr*125, 95, 120 ) showBanner: NO ];
        [ view.aSyncImage loadImageFromURL: r.img  previewUrl: nil cacheImage: YES ];
        view.label.text = [ r.title uppercaseString ];
        
        [self.scrollView addSubview: view ];
        view.backgroundColor = [ UIColor clearColor ];
        [ view release ];
        
        UIButton *button = [UIButton buttonWithType: UIButtonTypeCustom ];
        button.frame = CGRectMake( 5+colNr*107, 5+rowNr*125, 95, 120 );
        button.tag = [ r.id intValue ];
        [ button addTarget: self action: @selector( showRecepie: ) forControlEvents: UIControlEventTouchUpInside ];
        [self.scrollView addSubview: button ];
        
        colNr++;
        
        if ( colNr > 2 )
        {
            rowNr ++;
            colNr = 0;
        }
        n++;
    }
    
    self.scrollView.contentSize = CGSizeMake( scrollView.frame.size.width, rowNr * 125 + 125 );
    
    [fetchRequest release];
    [sortDescriptor release];
    [sortDescriptor2 release];
    [sortDescriptors release];
}

-(void) showRecepie: ( id ) sender
{
    UIButton *btn = ( UIButton *) sender;
    Recipe *r = [ self recepieWithId: [ NSNumber numberWithInt: btn.tag ] ];
    
    CookingViewController *cvc = [ [ CookingViewController alloc ] initWithNibName:@"CookingViewController"  bundle: nil ];
    
    cvc.recipe = r;    
    [ self.navigationController pushViewController: cvc animated: YES ];
    
    [ cvc release ];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [ self setupFavorites ];
    
    [ [ NSNotificationCenter defaultCenter ] addObserver: self selector:@selector( setupFavorites ) name: @"favorites" object: nil ];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [scrollView release];
    [super dealloc];
}
@end
