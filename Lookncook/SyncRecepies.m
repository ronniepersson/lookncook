//
//  SyncRecepies.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-06.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "SyncRecepies.h"
#import "SBJson.h"
#import "LCAppDelegate.h"
#import <CoreData/CoreData.h>
#import "Recipe.h"
#import "Category.h"

@implementation SyncRecepies
@synthesize categories;

-(void) doStartSync
{
    NSString *path = [[NSBundle mainBundle] pathForResource:
                      @"categories" ofType:@"plist"];
    self.categories = (NSArray *)[ [ NSDictionary dictionaryWithContentsOfFile: path ] valueForKey: @"Categories" ];
    
    NSURL *url = [ NSURL URLWithString: @"http://www.lookncook.se/api/recipe" ];
    [ self performSelectorInBackground: @selector( fetchUrl: ) withObject: url ];
}

//Call this is background
-(void) fetchUrl: ( NSURL * ) url
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init]; // Top-level pool
    
    NSError *error;
    
    NSStringEncoding encoding;
    
    NSString *result = [ NSString stringWithContentsOfURL: url usedEncoding: &encoding  error: &error ];
    
    
    
    
    //result = [ result stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    
   NSLog( @"result %@",  result );
    
    NSArray *recepies = [ result JSONValue ];
    
    [ self parseRecepies: recepies ];
    
    [ pool release ];
}


-( NSManagedObjectContext *) threadSafeCoreDataManagedContext
{
    LCAppDelegate *appDelegate = ( LCAppDelegate *) [ [ UIApplication sharedApplication ] delegate ];
    
    NSPersistentStoreCoordinator *coordinator = appDelegate.persistentStoreCoordinator;
    
    NSManagedObjectContext *context;
    
    if (coordinator != nil)
    {
        context = [[NSManagedObjectContext alloc] init];
        [context setPersistentStoreCoordinator:coordinator];
    }
    
    [context setMergePolicy: NSMergeByPropertyStoreTrumpMergePolicy ];
    
    NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
    [dnc addObserver:self selector:@selector(mergeContextChanges:) name:NSManagedObjectContextDidSaveNotification object: context ];
    
    return context;
}


-( NSString *) nameOfCategory: ( NSNumber *) id
{
    
    for ( NSDictionary *cat in self.categories )
    {
        for (  NSDictionary *subcat in [ cat valueForKey: @"Items" ] )
        {
            if ( [[[ subcat allKeys ] objectAtIndex: 0 ] intValue ] == [ id intValue ] )
            {
                return [[ subcat allValues ] objectAtIndex: 0];
            }
        }
    }
    return @"";
}


-( Recipe * ) newOrUsedRecepieWithId: ( NSNumber *) id andCategory: ( NSNumber *) catId inContext: ( NSManagedObjectContext *) context
{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Recipe" inManagedObjectContext: context ];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] 
                                        initWithKey:@"id" ascending:YES];
    
    
    NSArray *sortDescriptors = [[NSArray alloc] 
                                initWithObjects: sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate *predicate = [ NSPredicate predicateWithFormat: @"id==%@ AND category.id==%@", id, catId ];
    
    [ fetchRequest setPredicate: predicate ];
    
    NSError *error;
    NSArray *res = [ context executeFetchRequest: fetchRequest error: &error ];  
    
    if ( res != nil )
        if ( [ res count ] > 0 )
            return [ res objectAtIndex: 0 ];
    
    Recipe *newRecipe = [[ Recipe alloc ] initWithEntity:entity insertIntoManagedObjectContext: context ];

    newRecipe.id = id;
newRecipe.category = [ self newOrUsedCategoryWithId: catId inContext: context ];

[ fetchRequest release ];
[ sortDescriptor release ];
[ sortDescriptors release ];
    
    return newRecipe;
}

-(Category *) newOrUsedCategoryWithId: ( NSNumber *) id inContext: ( NSManagedObjectContext *) context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Category" inManagedObjectContext: context ];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] 
                                        initWithKey:@"id" ascending:YES];
    
    
    NSArray *sortDescriptors = [[NSArray alloc] 
                                initWithObjects: sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate *predicate = [ NSPredicate predicateWithFormat: @"id==%@", id ];
    
    [ fetchRequest setPredicate: predicate ];
    
    NSError *error;
    NSArray *res = [ context executeFetchRequest: fetchRequest error: &error ];  
    
    if ( res != nil )
        if ( [ res count ] > 0 )
            return [ res objectAtIndex: 0 ];
    
    Category *newCategory = [[ Category alloc ] initWithEntity:entity insertIntoManagedObjectContext: context ];
    
    newCategory.id = id;

[ fetchRequest release ];
[ sortDescriptor release ];
[ sortDescriptors release ];
    
    return newCategory;

}

-(void) parseRecepies: (NSArray *) recepies
{
    
    NSManagedObjectContext *context = [ self threadSafeCoreDataManagedContext ];
    
    if ( [ recepies count ] > 0 )
    {
        [TestFlight passCheckpoint:@"Did fetch recepies"];
    }
    
    for ( NSDictionary *recipeData in recepies )
    {
        NSNumber *id = [ NSNumber numberWithInt: [ [ recipeData valueForKey: @"id" ] intValue ]  ];
      
        NSDictionary *catsdata = [ recipeData valueForKey: @"kategorier" ];
        
        //one recepie for each category
        for ( NSDictionary *catdata in catsdata )
        {
            NSNumber *catid = [ NSNumber numberWithInt: [ [ catdata valueForKey: @"tid" ] intValue ]  ];
            Category *cat = [ self newOrUsedCategoryWithId: catid
                                                 inContext: context ];
            
            cat.name = [ self nameOfCategory: catid ]; 
            
        Recipe *recipe = [ self newOrUsedRecepieWithId: id andCategory: catid  inContext:context  ];
        
        recipe.title = [ recipeData valueForKey: @"title" ];
        recipe.created = [ NSNumber numberWithInt: [ [ recipeData valueForKey: @"created" ] intValue ]  ];
        recipe.cookingTime = [ recipeData valueForKey: @"tillagningstid" ];
        recipe.cooking = [ recipeData valueForKey: @"tillagning" ];
        recipe.ingredients =  [ recipeData valueForKey: @"ingredienser" ];
            recipe.tools = [ recipeData valueForKey: @"redskap" ];
            recipe.img = [ recipeData valueForKey: @"bild" ];
            recipe.thumbnail = [ recipeData valueForKey: @"thumbnail" ];
        recipe.video = [[ recipeData valueForKey: @"video_encoded" ] objectAtIndex: 0];
        recipe.category = cat;

        }
        
        
    }
    
    NSError *error;
    [ context save: &error ];
    
    
    [ self performSelectorOnMainThread: @selector( notifyDone ) withObject: nil waitUntilDone: NO ];
    
}
     
-( void) notifyDone
{
    
    [ [ NSNotificationCenter defaultCenter ] postNotificationName: @"LnCDidSync" object: nil ];
}

- (void)mergeContextChanges:(NSNotification *)notification{
    
    LCAppDelegate *appDelegate = ( LCAppDelegate *) [ [ UIApplication sharedApplication ] delegate ];
    
    SEL selector = @selector(mergeChangesFromContextDidSaveNotification:);
    [ appDelegate.managedObjectContext performSelectorOnMainThread:selector withObject:notification waitUntilDone:YES];
}

@end
