//
//  CardView.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-04-04.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface CardView : UIView
@property (nonatomic, retain ) IBOutlet AsyncImageView *aSyncImage;
@property ( nonatomic, retain ) IBOutlet UILabel *label;
- (id)initWithFrame:(CGRect)frame showBanner: ( bool ) showBanner;
@end
