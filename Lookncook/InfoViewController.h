//
//  InfoViewController.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-05-07.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface InfoViewController : UIViewController<SKProductsRequestDelegate>

@property (retain, nonatomic) IBOutlet UINavigationItem *infoNavigationItem;
@property (retain, nonatomic) IBOutlet UIWebView *infoWebView;
@property (retain, nonatomic) IBOutlet UIView *bgview;
- (IBAction)done:(id)sender;
- (IBAction)purchase:(id)sender;

@end
