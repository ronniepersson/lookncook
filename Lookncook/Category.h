//
//  Category.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-06.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Recipe;

@interface Category : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * categoryDescription;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSSet *recipes;
@end

@interface Category (CoreDataGeneratedAccessors)

- (void)addRecipesObject:(Recipe *)value;
- (void)removeRecipesObject:(Recipe *)value;
- (void)addRecipes:(NSSet *)values;
- (void)removeRecipes:(NSSet *)values;

@end
