//
//  FavoriteViewController.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-04-13.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;

@end
