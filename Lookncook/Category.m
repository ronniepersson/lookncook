//
//  Category.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-06.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "Category.h"
#import "Recipe.h"


@implementation Category

@dynamic name;
@dynamic categoryDescription;
@dynamic weight;
@dynamic id;
@dynamic recipes;

@end
