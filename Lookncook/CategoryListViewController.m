//
//  CategoryListViewController.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-08.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "CategoryListViewController.h"
#import "LCCategoryTableViewCell.h"
#import "LCSubCategoryTableViewCell.h"
#import "RecipeListViewController.h"
#import "LCAppDelegate.h"
#import "Recipe.h"
#import "AsyncImageView.h"
#import "CookingViewController.h"
#import "ShoppingListViewController.h"
#import "InfoViewController.h"
#import "GADBannerView.h"
#import "StoreObserver.h"

#define kPurchaseTypeNew 1
#define kPurchaseTypeRenew 2

@implementation CategoryListViewController
@synthesize tableListView;
@synthesize background;
@synthesize categories;
@synthesize titleLabel;
@synthesize latestRecipies;
@synthesize scrollView;
@synthesize searchTableView;
@synthesize searchVIew;
@synthesize searchBar;
@synthesize searchResult;
@synthesize subButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        selectedIndex = -1;
        self.searchResult = [[ NSArray alloc ] init];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void) showShoppingList
{
    ShoppingListViewController *svc = [ [ ShoppingListViewController alloc ] initWithNibName: @"ShoppingListViewController" bundle:nil ];
    [ self presentModalViewController: svc animated: YES ];
    [ svc release ];
}

-(void) showInfo
{
    InfoViewController *svc = [ [ InfoViewController alloc ] initWithNibName: @"InfoViewController" bundle:nil ];
    [ self presentModalViewController: svc animated: YES ];
    [ svc release ];
}


#pragma mark - UISearchBar Delegate

-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
   // self.scrollView.contentInset = UIEdgeInsetsMake( 42, 0, 0, 0 );
    self.searchVIew.hidden = NO;
    stopScroll = YES;
    self.scrollView.scrollEnabled = NO;
    self.searchBar.showsCancelButton = YES;
    self.searchTableView.frame = CGRectMake( self.searchTableView.frame.origin.x,self.searchTableView.frame.origin.y, 320, 170 );
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.scrollView.scrollEnabled = YES;
    self.searchBar.showsCancelButton = NO;
    self.searchVIew.hidden = YES;
    self.scrollView.contentInset = UIEdgeInsetsMake( 0, 0, 0, 0 );
    
    
    
    [ self.searchBar resignFirstResponder ];
}


-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    [ self.searchBar resignFirstResponder ];
    self.searchTableView.frame = CGRectMake( self.searchTableView.frame.origin.x,self.searchTableView.frame.origin.y, 320, 360 );
   
    
}

-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    
    LCAppDelegate *appDelegate = (LCAppDelegate *) [ [ UIApplication sharedApplication ] delegate ];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Recipe" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    [ fetchRequest setFetchLimit: 6];
    
    [ fetchRequest setPropertiesToFetch: [ NSArray arrayWithObjects: @"id" , nil ] ];
    [ fetchRequest setReturnsDistinctResults: YES ];
    [ fetchRequest setResultType: NSDictionaryResultType ];

    
    NSPredicate *predicate = [ NSPredicate predicateWithFormat: @"title contains[c] %@  OR ingredients contains[c] %@ ", searchText, searchText ];
    
    [ fetchRequest setPredicate: predicate ];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] 
                                        initWithKey:@"category" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] 
                                         initWithKey:@"title" ascending:YES]; 
    NSArray *sortDescriptors = [[NSArray alloc] 
                                initWithObjects:sortDescriptor,sortDescriptor2, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    self.searchResult = [ appDelegate.managedObjectContext executeFetchRequest: fetchRequest error: &error ];
    
    [fetchRequest release];
    [sortDescriptor release];
    [sortDescriptor2 release];
    [sortDescriptors release];
    
    
    [ self.searchTableView reloadData ];
}

#pragma mark - Scroll View Delegate


-(void) scrollViewDidScroll:(UIScrollView *)aScrollView
{
    if ( aScrollView == self.scrollView )
    {
        if ( self.scrollView.contentOffset.y < -42 )
        {
            self.scrollView.contentInset = UIEdgeInsetsMake( 42, 0, 0, 0 );
        }
    }
}




#pragma mark - Table view data source


-(int) numberOfSectionsInTableView:(UITableView *)tableView
{
    if ( tableView == searchTableView )
    {
        return 1;
    }
    
    return 1;
}

-(int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( tableView == searchTableView )
    {
        return [searchResult count];
    }
    
    
    return [ categories count ];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( tableView == searchTableView )
    {
        NSString *CellIdentifier = @"cell";
        
        LCSubCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[LCSubCategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        
        
        Recipe *recipe = [ self recepieWithId: [ [searchResult  objectAtIndex: indexPath.row ] objectForKey: @"id" ] ];
        
        cell.favPin.hidden = ![recipe.isFavorite boolValue];
        cell.textLabel.text = recipe.title;
        [cell.cardView.aSyncImage loadImageFromURL: recipe.img previewUrl: nil cacheImage: YES ];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;

    }
    
    static NSString *CellIdentifier = @"Cella";
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] 
                 initWithStyle:UITableViewCellStyleDefault
                 reuseIdentifier:CellIdentifier ] autorelease];
    }
    
    
    NSDictionary *cat = [ self.categories objectAtIndex: [ indexPath row ] ];
    
    cell.textLabel.font = [ UIFont fontWithName: @"Oswald-Bold" size: 24.0 ];
    cell.textLabel.text = [ [cat valueForKey: @"Category" ] uppercaseString ];
       // cell.icon.image = [ UIImage imageNamed: [ NSString stringWithFormat: @"icon_%d.png", [ indexPath section ] ] ];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
       

}


-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundView.alpha = 0.5;
    cell.textLabel.backgroundColor = [ UIColor clearColor ];
    cell.textLabel.textAlignment = UITextAlignmentCenter;
    
    cell.textLabel.font = [ UIFont fontWithName: @"Oswald-Bold" size: 24.0 ];
}


/*-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIImageView *footer = [ [ UIImageView alloc ] initWithImage: [ UIImage imageNamed: @"cell_shadow.png" ] ];
    [ footer setFrame: CGRectMake( 0, 0, footer.image.size.width, footer.image.size.height ) ];
    return footer;
}*/


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ( tableView == searchTableView )
    {
        Recipe *r = [ self recepieWithId: [ [ self.searchResult objectAtIndex: indexPath.row ] valueForKey: @"id" ]  ];
        
        CookingViewController *cvc = [ [ CookingViewController alloc ] initWithNibName:@"CookingViewController"  bundle: nil ];
        
        cvc.recipe = r;    
        [ self.navigationController pushViewController: cvc animated: YES ];
        
        [ cvc release ];
        return;
    }
    
    
        [ TestFlight passCheckpoint: @"Selected category" ];
        
        RecipeListViewController *cvc = [ [ RecipeListViewController alloc ] initWithNibName:@"RecipeListViewController"  bundle: nil ];
    
    //favorite
    if (  indexPath.row == 7 ) {
        cvc.showFavorites = YES;
    } else {
        
        NSDictionary *cat = [ self.categories objectAtIndex: [ indexPath row ] ];
        NSArray *items = [ cat valueForKey:@"Items" ];
        // NSString *subcat = [[[ items objectAtIndex: [ indexPath row ] - 1 ] allValues ] objectAtIndex: 0];
        
        
        NSMutableArray *subcats = [[ NSMutableArray alloc ] initWithCapacity: 10];
        for ( NSDictionary *subcat in items )
        {
            [subcats addObject: [[subcat allKeys ] objectAtIndex: 0 ]  ];
        }
        
        
        cvc.categories = subcats;
        //cvc.navigationItem.title = subcat;
    }
        
       
        
        
        [ self.navigationController pushViewController: cvc animated: YES ];
        
        [ cvc release ];
    
}



-(void) setupScroll
{
    for ( UIView *v in [self.latestRecipies subviews] )
        [ v removeFromSuperview ];
 
    LCAppDelegate *appDelegate = (LCAppDelegate *) [ [ UIApplication sharedApplication ] delegate ];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Recipe" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:10];
    
    
    [ fetchRequest setPropertiesToFetch: [ NSArray arrayWithObjects: @"id" , nil ] ];
    [ fetchRequest setReturnsDistinctResults: YES ];
    [ fetchRequest setResultType: NSDictionaryResultType ];
    [ fetchRequest setFetchLimit: 10 ];
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] 
                                        initWithKey:@"created" ascending: NO ];
    
    NSArray *sortDescriptors = [[NSArray alloc] 
                                initWithObjects:sortDescriptor,nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSError *error;
    NSArray *res = [ appDelegate.managedObjectContext executeFetchRequest: fetchRequest error: &error ];
    
    
    int n= 0;
    for ( NSDictionary *d in res  )
    {
        if ( n > 5 )
            break;
        
        Recipe *r = [ self recepieWithId: [ d objectForKey: @"id" ] ];
        
        CardView *view = [ [ CardView alloc ] initWithFrame: CGRectMake( 10 + n*120, 0, 105, 127 ) ];
        [ view.aSyncImage loadImageFromURL: r.thumbnail  previewUrl: nil cacheImage: YES ];
        view.label.text = [ r.title uppercaseString ];
        [self.latestRecipies addSubview: view ];
        view.backgroundColor = [ UIColor clearColor ];
        [ view release ];
        
        UIButton *button = [UIButton buttonWithType: UIButtonTypeCustom ];
        button.frame = CGRectMake( 10 + n*120, 0, 105, 127 );
        button.tag = [ r.id intValue ];
        [ button addTarget: self action: @selector( showRecepie: ) forControlEvents: UIControlEventTouchUpInside ];
        [self.latestRecipies addSubview: button ];
        
        n++;
    }
    self.latestRecipies.contentSize = CGSizeMake( 10 + n*120, latestRecipies.frame.size.height );
    
}

-(void) showRecepie: ( id ) sender
{
    UIButton *btn = ( UIButton *) sender;
    Recipe *r = [ self recepieWithId: [ NSNumber numberWithInt: btn.tag ] ];
    
    CookingViewController *cvc = [ [ CookingViewController alloc ] initWithNibName:@"CookingViewController"  bundle: nil ];
    
    cvc.recipe = r;    
    [ self.navigationController pushViewController: cvc animated: YES ];
    
    [ cvc release ];

}

-(void) removeAd
{
    [[ self.view viewWithTag: 99111 ] removeFromSuperview ];
    self.scrollView.frame = CGRectMake( self.view.frame.origin.x,self.view.frame.origin.x, self.view.frame.size.width, 480-20-44 );
    self.latestRecipies.frame = CGRectMake(0, 40, 320, 127 );
    self.subButton.hidden = YES;
}

-(void) addAdToView
{
    
    self.scrollView.frame = CGRectMake( self.view.frame.origin.x,self.view.frame.origin.x, self.view.frame.size.width, 480-20-44-44 );
    self.latestRecipies.frame = CGRectMake(0, 40, 320, 127 );
    self.subButton.hidden = NO;
    GADBannerView *adView =
    [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    adView.rootViewController = self;
    adView.adUnitID = @"a14fa63600bb900";
    adView.frame = CGRectMake( 0, 480-20-44-kGADAdSizeBanner.size.height, kGADAdSizeBanner.size.width, kGADAdSizeBanner.size.height);
    adView.tag = 99111;
    // Place the ad view onto the screen.
    [self.view addSubview:adView];
    [adView release];
    
    
    // Request an ad without any additional targeting information.
    [adView loadRequest:nil];
}

- (IBAction)purchase:(id)sender {
    
    if ([SKPaymentQueue canMakePayments]) {
      
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: @"Look n Cook" message: @"Ta bort reklam" delegate: self cancelButtonTitle: @"Avbryt" otherButtonTitles: @"Köp", @"Återställ tidigare köp" , nil ];
        [ alert show ];
        [ alert release ];
    
     
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: @"Look n Cook" message: @"Köp från den här enheten är avaktiverat" delegate: nil cancelButtonTitle: nil otherButtonTitles: @"Ok" , nil ];
        [alert show];
        [ alert release ];
        
    }
}

#pragma mark - UIAlertViewDelegate

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    purchaseType = buttonIndex;
    
    SKProductsRequest *request= [ [ SKProductsRequest alloc ] initWithProductIdentifiers: [ NSSet setWithObject: kNoAdProductIdentifier ] ];
    
    request.delegate = self;
    [request start];
}

#pragma mark - SKProductsRequestsDelegate -

-(void) productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSArray *myProducts = response.products;
    
    
    SKProduct *selectedProduct = [ myProducts objectAtIndex: 0 ];
    SKPayment *payment = [SKPayment paymentWithProduct:selectedProduct];
    
    switch ( purchaseType) {
        case kPurchaseTypeNew:
            [[SKPaymentQueue defaultQueue] addPayment:payment];
            break;
            
        case kPurchaseTypeRenew:
            [[SKPaymentQueue defaultQueue] restoreCompletedTransactions ];
            
            break;
            
        default:
            break;
    }
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ( ![ StoreObserver skipAd ] )
    {
        [ self addAdToView ];
    } else {
        [ self removeAd ];
    }
    
    //Testflight
    [ TestFlight passCheckpoint: NSStringFromClass( [ self class ] ) ];
    
    //Observers
    [ [ NSNotificationCenter defaultCenter ] addObserver: self selector:@selector( setupScroll ) name: @"LnCDidSync" object: nil ];
    
    
    //Load data
    NSString *path = [[NSBundle mainBundle] pathForResource:
                      @"categories" ofType:@"plist"];
    self.categories =[ [ NSDictionary dictionaryWithContentsOfFile: path ] valueForKey: @"Categories" ];
    
    
    //UI
    self.titleLabel.font = [ UIFont fontWithName: @"Oswald-Bold" size: 20.0 ];
   
    [self.tableListView setShowsVerticalScrollIndicator: NO ];
    [ self setupScroll ];
    self.scrollView.contentSize = CGSizeMake( 320, 590 );
    
    [ self.navigationItem setTitleView: [[ UIImageView alloc ] initWithImage: [UIImage imageNamed: @"lclogo.png"]]];
    
    UIBarButtonItem *btn = [[ UIBarButtonItem alloc ] initWithImage: [ UIImage imageNamed:@"shoppinglist.png"] style:UIBarButtonItemStylePlain target: self action: @selector( showShoppingList ) ];
    
    self.navigationItem.rightBarButtonItem = btn;
    
    UIBarButtonItem *btn2 = [[ UIBarButtonItem alloc ] initWithTitle: @"info" style:UIBarButtonItemStylePlain target: self action: @selector( showInfo ) ];
    
    self.navigationItem.leftBarButtonItem = btn2;
    [btn release];
    [btn2 release];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector( removeAd ) name:kNoAdProductBought object: nil ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector( addAdToView ) name:kNoAdProductEnded object: nil ];
    
    

   
    
    
}


- (void)viewDidUnload
{
    [self setTableListView:nil];
    [self setBackground:nil];
    [self setTitleLabel:nil];
    [self setLatestRecipies:nil];
    [self setScrollView:nil];
    [self setSearchTableView:nil];
    [self setSearchVIew:nil];
    [self setSearchBar:nil];
    [self setSubButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return interfaceOrientation == UIInterfaceOrientationPortrait;
}

-(void) viewWillAppear:(BOOL)animated
{
    [ self willAnimateRotationToInterfaceOrientation: self.interfaceOrientation duration: 0 ];
}


-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
    
    int width = 320;
    int height = 480;
    
    if ( toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight )
    {
        width = 480;
        height = 320;
    }
    
    
    self.background.frame = CGRectMake( 0, 0, width, height);
    //self.tableListView.frame = CGRectMake( width/2-320/2 , self.tableListView.frame.origin.y , 320 , height-25-84);
}



- (void)dealloc {
    
    [tableListView release];
    [background release];
    [titleLabel release];
    [latestRecipies release];
    [scrollView release];
    [searchTableView release];
    [searchVIew release];
    [searchBar release];
    [subButton release];
    [super dealloc];
}
@end
