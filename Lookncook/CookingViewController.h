//
//  CookingViewController.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-10.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipe.h"
#import "AsyncImageDelegate.h"
#import <MediaPlayer/MediaPlayer.h>
@interface CookingViewController : UIViewController<AsyncImageDelegate,UIScrollViewDelegate>
{
    bool pageControlUsed;
}
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIScrollView *pageControlView;
@property (retain, nonatomic) IBOutlet UIPageControl *pageControl;
@property (retain, nonatomic) IBOutlet UIView *buttonView;
@property (retain, nonatomic) IBOutlet UIView *buttonViewVertical;
@property (retain, nonatomic) IBOutlet UILabel *titleHeader;

@property (retain, nonatomic) IBOutlet UIView *shoppingListView;
@property (retain, nonatomic) IBOutlet UITextView *shoppingListList;
@property (retain, nonatomic) IBOutlet UIView *shoppingListContentView;
@property (retain, nonatomic) IBOutlet UILabel *shoppingListHeader;

@property (retain, nonatomic) IBOutlet UIView *videoRecepieView;
@property (retain, nonatomic) IBOutlet UIView *videoRecepiePreview;
@property (retain, nonatomic) IBOutlet UILabel *videoText;

@property (retain, nonatomic) IBOutlet UIView *stepByStepView;
@property (retain, nonatomic) IBOutlet UIView *stepByStepContentView;
@property (retain, nonatomic) IBOutlet UILabel *steByStepHeader;
@property (retain, nonatomic) IBOutlet UITextView *stepByStepText;

@property (retain, nonatomic) IBOutlet UIView *orientationSign;
@property (retain, nonatomic) IBOutlet UIButton *favButton;
- (IBAction)selectStepByStep:(id)sender;
- (IBAction)selectShoppingList:(id)sender;
- (IBAction)selectVideoRecepie:(id)sender;
- (IBAction)startPlay:(id)sender;
- (IBAction)changeSection:(id)sender;
- (IBAction)setAsFav:(id)sender;
- (IBAction)addToShoppingList:(id)sender;

@property ( retain, nonatomic ) Recipe *recipe;
@property (nonatomic, retain) MPMoviePlayerController *player;
@property NSTimeInterval lastPlaybackTime;
@end
