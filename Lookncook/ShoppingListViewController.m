//
//  ShoppingListViewController.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-28.
//  Copyright (c) 2012 Binofo. All rights reserved.
//


#import "ShoppingListViewController.h"
#import "LCAppDelegate.h"
#import "Recipe.h"
#import "LCShoppingItemTableViewCell.h"
#import "CookingViewController.h"
#import "ShoppingListItem.h"
#import "LCAppDelegate.h"

#define ACTIONSHEETBUTTON_DELETECHECKED 0
#define ACTIONSHEETBUTTON_DELETEALL 1

@implementation ShoppingListViewController

@synthesize infoNavigationItem;
@synthesize actionButton;
@synthesize tableListView;
@synthesize fetchedResultsController;
@synthesize background;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(IBAction ) showOptions: ( id ) sender
{
    UIActionSheet *actionSheet = [ [ UIActionSheet alloc ] initWithTitle: @"Alternativ" delegate: self cancelButtonTitle: @"Avbryt" destructiveButtonTitle: nil otherButtonTitles: @"Ta bort alla avkryssade", @"Töm inköpslistan" , nil ];
    
    [ actionSheet showFromBarButtonItem: actionButton  animated: YES ];
    [ actionSheet release ];
}

- (IBAction)done:(id)sender {
    [ self dismissModalViewControllerAnimated: YES ];
}

#pragma mark - Actionsheetdelegate

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    LCAppDelegate *appDelegate = ( LCAppDelegate * ) [ [ UIApplication sharedApplication ] delegate ];
    NSError *error;
    NSPredicate *predicate;
    NSArray *objectsToDelete;
    
    switch ( buttonIndex ) {
        case ACTIONSHEETBUTTON_DELETECHECKED:
            
            [ TestFlight passCheckpoint: @"Deleted checked items in shoppinglist" ];
            
            predicate = [ NSPredicate predicateWithFormat: @"self.checked==%@", [ NSNumber numberWithBool: YES ] ];
            
            objectsToDelete = [ [ fetchedResultsController fetchedObjects ] filteredArrayUsingPredicate: predicate ];
            
            break;
            
        case ACTIONSHEETBUTTON_DELETEALL:
            
            [ TestFlight passCheckpoint: @"Deleted all items in shoppinglist" ];
            
            objectsToDelete = [ fetchedResultsController fetchedObjects ];
            break;
            
        default:
            objectsToDelete = [ NSArray array ];
            break;
    }
    
    for ( NSManagedObject *obj in objectsToDelete )
        [ appDelegate.managedObjectContext deleteObject: obj  ];
    
    [ appDelegate.managedObjectContext save: &error ];
    [ self.tableListView reloadData ];
}

#pragma mark -
#pragma mark NSFetchedResultsControllerDelegate

- (NSFetchedResultsController *)fetchedResultsController {
    
    if ( fetchedResultsController != nil) {
        return fetchedResultsController;
    }
    
    LCAppDelegate *appDelegate = (LCAppDelegate *) [ [ UIApplication sharedApplication ] delegate ];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ShoppingListItem" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] 
                                        initWithKey:@"item" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] 
                                initWithObjects:sortDescriptor,nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *aFetchedResultsController = 
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext: appDelegate.managedObjectContext 
                                          sectionNameKeyPath: nil cacheName:nil];
    
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    [aFetchedResultsController release];
    [fetchRequest release];
    [sortDescriptor release];
    [sortDescriptors release];
    
    return fetchedResultsController;
} 



- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    [ self.tableListView reloadData ];
}

#pragma mark - Table view data source



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return[ [fetchedResultsController sections] count ];
}

-(BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [ [fetchedResultsController sections] objectAtIndex: section ];
    
    return  [ sectionInfo numberOfObjects ];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    if (self.editing) {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleNone;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CellIdentifier = @"Cella";
    
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] 
                 initWithStyle:UITableViewCellStyleDefault
                 reuseIdentifier:CellIdentifier ] autorelease];
    }
    
    ShoppingListItem *item = [ fetchedResultsController objectAtIndexPath: indexPath ];
    
    cell.textLabel.text = item.item;
    
    cell.textLabel.font = [ UIFont fontWithName: @"Oswald-Bold" size: 24.0 ];
    cell.textLabel.text = item.item;
    
    if ( [item.checked boolValue] )
        cell.textLabel.alpha = 0.5;
    else
        cell.textLabel.alpha = 1.0;
    
    // cell.icon.image = [ UIImage imageNamed: [ NSString stringWithFormat: @"icon_%d.png", [ indexPath section ] ] ];
    
    return cell;
    
    
}


-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundView.alpha = 0.5;
    cell.textLabel.backgroundColor = [ UIColor clearColor ];
    cell.textLabel.textAlignment = UITextAlignmentCenter;
    
    cell.textLabel.font = [ UIFont fontWithName: @"Oswald-Bold" size: 14.0 ];
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [ TestFlight passCheckpoint: @"Did check/uncheck in shoppinglist" ];
    
    ShoppingListItem *item = [ fetchedResultsController objectAtIndexPath: indexPath ];
    item.checked = [ NSNumber numberWithBool:![ item.checked boolValue ] ];
   
    LCAppDelegate *appDelegate = ( LCAppDelegate * ) [ [ UIApplication sharedApplication ] delegate ];
    NSError *error;
    [ appDelegate.managedObjectContext save: &error ];
    
    [ self.tableListView reloadData ];
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [ TestFlight passCheckpoint: NSStringFromClass( [ self class ] ) ];
    
    [ self.infoNavigationItem setTitleView: [[ UIImageView alloc ] initWithImage: [UIImage imageNamed: @"lclogo.png"]]];
    
    NSError *error;
    [ self.fetchedResultsController performFetch: &error ];
    // Do any additional setup after loading the view from its nib.
}


- (void)viewDidUnload
{
    [self setTableListView:nil];
    [self setBackground:nil];
    [self setActionButton:nil];
    [self setInfoNavigationItem:nil];
    [super viewDidUnload];
    self.fetchedResultsController = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void) viewWillAppear:(BOOL)animated
{
    [ self willAnimateRotationToInterfaceOrientation: self.interfaceOrientation duration: 0 ];
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    
    int width = 320;
    int height = 480 - self.tableListView.frame.origin.y - 20;
    
    if ( toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight )
    {
        width = 480;
        height = 320 - self.tableListView.frame.origin.y - 20;
    }
    
    self.background.frame = CGRectMake( 0, self.tableListView.frame.origin.y, width, height);
    
    self.tableListView.frame = CGRectMake( self.tableListView.frame.origin.x , self.tableListView.frame.origin.y , width , height);
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return interfaceOrientation == UIInterfaceOrientationPortrait;
}

- (void)dealloc {
    self.fetchedResultsController = nil;
    [tableListView release];
    [background release];
    [actionButton release];
    [infoNavigationItem release];
    [super dealloc];
}
@end
