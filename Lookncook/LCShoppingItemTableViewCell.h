//
//  LCShoppingItemTableViewCell.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-02-08.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCShoppingItemTableViewCell : UITableViewCell

@property (nonatomic, retain) UIImageView *background;
@end
