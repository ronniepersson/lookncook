//
//  Recipe.h
//  Lookncook
//
//  Created by Ronnie Persson on 2012-05-11.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Category;

@interface Recipe : NSManagedObject

@property (nonatomic, retain) NSString * cooking;
@property (nonatomic, retain) NSString * cookingTime;
@property (nonatomic, retain) NSNumber * created;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * img;
@property (nonatomic, retain) NSString * ingredients;
@property (nonatomic, retain) NSNumber * isFavorite;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * tools;
@property (nonatomic, retain) NSString * video;
@property (nonatomic, retain) NSString * thumbnail;
@property (nonatomic, retain) Category *category;

@end
