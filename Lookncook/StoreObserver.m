//
//  StoreObserver.m
//  Lookncook
//
//  Created by Ronnie Persson on 2012-05-08.
//  Copyright (c) 2012 Binofo. All rights reserved.
//

#import "StoreObserver.h"
#import "NSData+Base64.h"
#import "ASIFormDataRequest.h"
#import "SBJson.h"


@implementation StoreObserver


-(void) boughtProduct
{
    NSUserDefaults *defaults = [[ NSUserDefaults standardUserDefaults ] init ];
    
    [ defaults setValue: kNoAdProductEnded forKey: @"boughtProduct" ];
    [ defaults synchronize ];
    [[ NSNotificationCenter defaultCenter ] postNotificationName: kNoAdProductBought object: nil ];
}

+(BOOL) skipAd
{
    NSUserDefaults *defaults = [[ NSUserDefaults standardUserDefaults ] init ];
    if ( [ defaults valueForKey: @"boughtProduct" ] == nil )
    {
        return  NO;
    }
    
    if ( [ [ defaults valueForKey: @"boughtProduct" ] isEqualToString: kNoAdProductIdentifier ] )
        return YES;
    
    return NO;
}


//Sends stored recipts to apple for verification
-(void) checkForValidReceipt
{
   /* NSUserDefaults *defaults = [[ NSUserDefaults standardUserDefaults ] init ];

    NSArray *transactions =  [ defaults valueForKey: @"transactions" ];
    
    //set to nil before validation
    [ defaults setValue: nil forKey: @"boughtProduct" ];
    [ defaults synchronize ];
    
    if ( transactions == nil )
    {
        [ defaults setValue: nil forKey: @"boughtProduct" ];
        [ [ NSNotificationCenter defaultCenter ] postNotificationName: kNoAdProductEnded object: nil ];
        return;
    }
    
    for ( NSData *transactionReceipt in transactions )
    {
        NSString *recieptBytes = [transactionReceipt base64EncodingWithLineLength:0];
        NSString *jsonstring = [ NSString stringWithFormat: @"{\"receipt-data\" : \"%@\",\"password\":\"%@\"}", recieptBytes, kValidationSecret ];
        
        NSURL *url = [NSURL URLWithString: kValidationServer];
        __block ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request appendPostData: [ jsonstring dataUsingEncoding: NSUTF8StringEncoding ] ];
        [request setCompletionBlock:^{
            // Use when fetching text data
            NSString *responseString = [request responseString];
            
            NSDictionary *dict = [ responseString JSONValue ];
            
            //if status == 0 then this reciept is active and valid
            if ( [[ dict valueForKey: @"status" ] intValue] == 0 )
            {
                NSLog( @"Found valid reciept" );
                [ self performSelectorOnMainThread: @selector( boughtProduct ) withObject: nil waitUntilDone: NO ];
            } else {
                NSLog( @"Error status: %@", [ dict valueForKey: @"status" ]  );
            }
            
        }];
        [request setFailedBlock:^{
            NSError *error = [request error];
        }];
        [request startAsynchronous];
    }*/
    
}

-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog( @"Finished restore" );
}

-(void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error:(SKPaymentQueue *)queue
{
    NSLog( @"Error restoring: %@", [error description]);
}



- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    }
}

- (void) recordTransaction: (SKPaymentTransaction *)transaction
{
    NSLog( @"recorded transaction" );
    
    NSUserDefaults *defaults = [[ NSUserDefaults standardUserDefaults ] init ];
    
    NSMutableArray *transactions = [ [ NSMutableArray alloc ] init ];
    
    if ( [ defaults valueForKey: @"transactions" ] != nil )
    {
        for ( SKPaymentTransaction *oldTransaction in [ defaults valueForKey: @"transactions" ] )
        {
            [ transactions addObject: oldTransaction ];
        }
    }
    
    
    [ transactions addObject: transaction.transactionReceipt ];
    
     [ defaults setValue: transactions forKey: @"transactions" ];

    
    [ defaults synchronize ];
}

-(void) provideContent: ( NSString *) productIdentifier
{
    NSLog( @"bought: %@", productIdentifier );
    
    NSUserDefaults *defaults = [[ NSUserDefaults standardUserDefaults ] init ];
    [ defaults setValue: productIdentifier forKey: @"boughtProduct" ];
    [ defaults synchronize ];
    
    [[ NSNotificationCenter defaultCenter ] postNotificationName: kNoAdProductBought object: nil ];
}

- ( void ) completeTransaction: (SKPaymentTransaction *)transaction
{
    // Your application should implement these two methods.
    [self recordTransaction:transaction];
    [self provideContent:transaction.payment.productIdentifier];
    
    // Remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- ( void ) failedTransaction: (SKPaymentTransaction *)transaction
{
    if (transaction.error.code != SKErrorPaymentCancelled) {
        NSLog( @"Transaction failed: %@", [ transaction.error description ] );
    }
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- ( void ) restoreTransaction: (SKPaymentTransaction *)transaction
{
    [self recordTransaction: transaction];
    [self provideContent: transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    
}



@end
